// 2019 by Stephan Roslen

#include <iostream>
#include <type_traits>

#define CONCAT(a,b) CONCAT_(a,b)
#define CONCAT_(a,b) a ## b
#define CHECKVARNAME CONCAT(checkvar_,__COUNTER__)

using inttype = unsigned int;

namespace util {

template <typename T>
struct debug_t;

template <typename Type0, typename Type1, typename SFINAE = void>
struct samecheck_t;

template <typename Type0, typename Type1>
struct samecheck_t<
    Type0,
    Type1,
    std::enable_if_t<std::is_same_v<Type0, Type1>>> {};
}

namespace typelist {

template<typename... Types>
struct typelist;

template<typename Typelist0, typename Typelist1>
struct concat;

template<typename... Types0, typename... Types1>
struct concat<typelist<Types0...>, typelist<Types1...>> {
  using type = typelist<Types0..., Types1...>;
};

template<typename Typelist0, typename Typelist1>
using concat_t = typename concat<Typelist0, Typelist1>::type;

template<typename Typelist>
struct headtail;

template<typename TypeHead, typename... TypeTail>
struct headtail<typelist<TypeHead, TypeTail...>> {
  using head = typelist<TypeHead>;
  using tail = typelist<TypeTail...>;
};

template<typename T, T... values>
using integral_typelist = typelist<std::integral_constant<T, values>...>;

template<inttype... is>
using intlist = integral_typelist<inttype, is...>;

template<char... chars>
using charlist = integral_typelist<char, chars...>;

template<
    typename Tlhs,
    typename Trhs,
    typename = std::enable_if_t<
        std::is_same_v<typename Tlhs::value_type, typename Trhs::value_type>>>
struct integral_less_equal : std::bool_constant<Tlhs::value <= Trhs::value> {};

template<typename Trhs>
struct integral_less_equal_than {
  template<typename Tlhs>
  using type = integral_less_equal<Tlhs, Trhs>;
};

template<
    typename Tlhs,
    typename Trhs,
    typename = std::enable_if_t<
        std::is_same_v<typename Tlhs::value_type, typename Trhs::value_type>>>
struct integral_greater : std::bool_constant<(Tlhs::value > Trhs::value)> {};

template<typename Trhs>
struct integral_greater_than {
  template<typename Tlhs>
  using type = integral_greater<Tlhs, Trhs>;
};

template<
    template <typename, typename> typename Function,
    typename Initial,
    typename Typelist>
struct foldl;

template<
    template <typename, typename> typename Function,
    typename Initial>
struct foldl<Function, Initial, typelist<>> {
  using type = Initial;
};

template<
    template <typename, typename> typename Function,
    typename Initial,
    typename Typelist>
using foldl_t = typename foldl<Function, Initial, Typelist>::type;

template<
    template <typename, typename> typename Function,
    typename Initial,
    typename TypeHead,
    typename... TypeTail>
struct foldl<Function, Initial, typelist<TypeHead, TypeTail...>> {
  using ResidualList = typelist<TypeTail...>;
  using FoldedList = foldl_t<Function, TypeHead, ResidualList>;
  using type = typename Function<Initial, FoldedList>::type;
};

template<
    template <typename> typename Function,
    typename Typelist>
struct map;

template<
    template <typename> typename Function>
struct map<Function, typelist<>> {
  using type = typelist<>;
};

template<
    template <typename> typename Function,
    typename Typelist>
using map_t = typename map<Function, Typelist>::type;

template<
    template <typename> typename Function,
    typename TypeHead,
    typename... TypeTail>
struct map<Function, typelist<TypeHead, TypeTail...>> {
  using MappedHead = typename Function<TypeHead>::type;
  using MappedTail = map_t<Function, typelist<TypeTail...>>;
  using type = concat_t<typelist<MappedHead>, MappedTail>;
};

template<
    typename Typelist,
    template <typename> typename Condition,
    typename SFINAE = void>
struct filter;

template<template <typename> typename Condition>
struct filter<typelist<>, Condition, void> {
  using type = typelist<>;
};

template<typename Typelist, template <typename> typename Condition>
using filter_t = typename filter<Typelist, Condition>::type;

template<
    template <typename> typename Condition,
    typename TypeHead,
    typename... TypeTail>
struct filter<
    typelist<TypeHead, TypeTail...>,
    Condition,
    std::enable_if_t<Condition<TypeHead>::value>> {
  using type = concat_t<
      typelist<TypeHead>,
      filter_t<typelist<TypeTail...>, Condition>>;
};

template<
    template <typename> typename Condition,
    typename TypeHead,
    typename... TypeTail>
struct filter<
    typelist<TypeHead, TypeTail...>,
    Condition,
    std::enable_if_t<!Condition<TypeHead>::value>> {
  using type = filter_t<typelist<TypeTail...>, Condition>;
};

template<typename Typelist>
struct quicksort;

template<>
struct quicksort<typelist<>> {
  using type = typelist<>;
};

template<typename Typelist>
using quicksort_t = typename quicksort<Typelist>::type;

template<typename TypeHead, typename... TypeTail>
struct quicksort<typelist<TypeHead, TypeTail...>> {
  using Pivot = TypeHead;
  using ResidualList = typelist<TypeTail...>;

  template<typename Tlhs>
  using LETF = integral_less_equal<Tlhs, Pivot>;

  template<typename Tlhs>
  using GTF = integral_greater<Tlhs, Pivot>;

  using TypeListLET = filter_t<ResidualList, LETF>;
  using TypeListGT = filter_t<ResidualList, GTF>;

  using TypeListLETSorted = quicksort_t<TypeListLET>;
  using TypeListGTSorted = quicksort_t<TypeListGT>;

  using type = concat_t<
      concat_t<TypeListLETSorted, typelist<Pivot>>, TypeListGTSorted>;
};

}

namespace util {

template<typename T, typename SFINAE = void>
struct bcd;

template<typename T>
struct bcd<T, typename std::enable_if_t<(10 > T::value)>> {
  using type = typelist::typelist<T>;
};

template<typename T>
using bcd_t = typename bcd<T>::type;

template<typename T>
struct bcd<T, typename std::enable_if_t<!(10 > T::value)>> {
  using Digit = typename std::integral_constant<
      typename T::value_type,
      T::value % 10>;
  using Residual = typename std::integral_constant<
      typename T::value_type,
      T::value / 10>;
  using type = typelist::concat_t<bcd_t<Residual>, typelist::typelist<Digit>>;
};

template<typename T>
struct bcddigit2char {
  using type = std::integral_constant<char, ((char) T::value) + '0'>;
};

template<typename T>
using bcddigit2char_t = typename bcddigit2char<T>::type;

template<typename T>
struct itocharlist {
  template <typename Ttfun>
  using TFun = bcddigit2char_t<Ttfun>;
  using type = typelist::map_t<TFun, bcd_t<T>>;
};

template<typename T>
using itocharlist_t = typename itocharlist<T>::type;

template<typename Separator, typename CharlistList>
struct join;

template<typename Separator>
struct join<Separator, typelist::typelist<>> {
  using type = typelist::charlist<>;
};

template<typename Separator, typename TypeHead, typename... TypeTail>
struct join<Separator, typelist::typelist<TypeHead, TypeTail...>> {
  using ResidualList = typelist::typelist<TypeTail...>;
  template<typename Tlhs, typename Trhs>
  using FoldFun = typename typelist::concat<
      Tlhs,
      typelist::concat_t<Separator, Trhs>>;
  using type = typelist::foldl_t<FoldFun, TypeHead, ResidualList>;
};

template<typename Separator, typename CharlistList>
using join_t = typename join<Separator, CharlistList>::type;

template<typename T>
struct charlistcstr;

template<char... cs>
struct charlistcstr<typelist::charlist<cs...>> {
  static constexpr char cstr[] = {cs..., '\0'};
};

template<typename T>
constexpr auto charlistcstr_v = charlistcstr<T>::cstr;

}

namespace implementation {

template<typename T>
struct integerssortedcharlistlist {
  using Sorted = typelist::quicksort_t<T>;
  template <typename Tfpar>
  using TFun = typename util::itocharlist<Tfpar>;
  using type = typelist::map_t<TFun, Sorted>;
};

template<typename T>
using integerssortedcharlistlist_t
    = typename integerssortedcharlistlist<T>::type;

template<typename Sep, typename Fin, typename T>
struct integerssortedcharlist {
  using CLL = integerssortedcharlistlist_t<T>;
  using type = typelist::concat_t<util::join_t<Sep, CLL>, Fin>;
};

template<typename Sep, typename Fin, typename T>
using integerssortedcharlist_t
    = typename integerssortedcharlist<Sep, Fin, T>::type;

}

#ifdef DOCHECK
util::samecheck_t<
    typelist::typelist<int, char, double, bool>,
    typelist::concat_t<
        typelist::typelist<int, char>,
        typelist::typelist<double, bool>>> CHECKVARNAME;

util::samecheck_t<
    typelist::intlist<3, 2, 1>,
    typelist::filter_t<
        typelist::intlist<6, 3, 7, 2, 8, 1, 9>,
        typelist::integral_less_equal_than<
            std::integral_constant<inttype, 3>>::type>> CHECKVARNAME;

util::samecheck_t<
    typelist::intlist<7, 8, 9>,
    typelist::filter_t<
        typelist::intlist<1, 7, 2, 8, 3, 9, 4>,
        typelist::integral_greater_than<
            std::integral_constant<inttype, 6>>::type>> CHECKVARNAME;

util::samecheck_t<
    typelist::intlist<1, 2, 3, 4, 5>,
    typelist::quicksort_t<typelist::intlist<3, 1, 4, 2, 5>>> CHECKVARNAME;

util::samecheck_t<
    typelist::intlist<5, 1, 2>,
    util::bcd_t<std::integral_constant<inttype, 512>>> CHECKVARNAME;

util::samecheck_t<
    typelist::intlist<7>,
    util::bcd_t<std::integral_constant<inttype, 7>>> CHECKVARNAME;

util::samecheck_t<
    typelist::intlist<0>,
    util::bcd_t<std::integral_constant<inttype, 0>>> CHECKVARNAME;

util::samecheck_t<
    typelist::charlist<'5', '1', '2'>,
    util::itocharlist_t<std::integral_constant<inttype, 512>>> CHECKVARNAME;

util::samecheck_t<
    implementation::integerssortedcharlistlist_t<typelist::intlist<42,23,93>>,
    typelist::typelist<
        typelist::charlist<'2', '3'>,
        typelist::charlist<'4', '2'>,
        typelist::charlist<'9', '3'>>> CHECKVARNAME;

util::samecheck_t<
    implementation::integerssortedcharlist_t<
        typelist::charlist<'a'>,
        typelist::charlist<'b'>,
        typelist::intlist<42,23,93>>,
    typelist::charlist<
        '2', '3', 'a', '4', '2', 'a', '9', '3', 'b'>> CHECKVARNAME;
#endif

using unsorted = typelist::intlist<
    8, 36, 63, 98, 59, 37, 74, 38, 62, 70, 50, 16, 14, 83, 12, 3, 84, 93, 64,
    88, 80, 68, 20, 85, 29, 32, 52, 60, 99, 5, 73, 30, 67, 13, 90, 78, 19, 97,
    66, 45, 69, 89, 1, 65, 86, 71, 40, 57, 25, 7, 43, 46, 54, 61, 17, 10, 28,
    11, 41, 35, 91, 21, 81, 2, 23, 79, 44, 31, 51, 49, 0, 94, 77, 42, 39, 9, 48,
    87, 53, 15, 24, 6, 18, 56, 82, 34, 72, 55, 22, 75, 96, 26, 47, 27, 58, 76,
    33, 92, 95, 4>;
using output = implementation::integerssortedcharlist_t<
    typelist::charlist<',', ' '>,
    typelist::charlist<'\n'>,
    unsorted>;
constexpr auto outputstr = util::charlistcstr_v<output>;

int main() {
  std::cout << outputstr;
  return 0;
}